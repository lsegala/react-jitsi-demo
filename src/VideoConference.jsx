import React from "react";

const VideoConference = () => {
  const jitsiContainerId = "jitsi-container-id";
  const [jitsi, setJitsi] = React.useState({});

  const loadJitsiScript = () => {
    let resolveLoadJitsiScriptPromise = null;

    const loadJitsiScriptPromise = new Promise(resolve => {
      resolveLoadJitsiScriptPromise = resolve;
    });

    const script = document.createElement("script");
    script.src = "https://meet.jit.si/external_api.js";
    script.async = true;
    script.onload = () => resolveLoadJitsiScriptPromise(true);
    document.body.appendChild(script);

    return loadJitsiScriptPromise;
  };

  const initialiseJitsi = async () => {
    if (!window.JitsiMeetExternalAPI) {
      await loadJitsiScript();
    }

    const _jitsi = new window.JitsiMeetExternalAPI("meet.jit.si", {
      parentNode: document.getElementById(jitsiContainerId),
      roomName: '92875d0a-e318-11ea-87d0-0242ac130003',
      height: 700
    });
    _jitsi.addEventListener('endpointTextMessageReceived', evt => {
      console.log('endpointTextMessageReceived');
      console.log(evt);
    });
    _jitsi.addEventListener('incomingMessage', evt => {
      console.log('incomingMessage');
      console.log(evt);
    });
    _jitsi.addEventListener('outgoingMessage', evt => {
      console.log('outgoingMessage');
      console.log(evt);
    });
    _jitsi.addEventListener('participantJoined', evt => {
      console.log('participantJoined');
      console.log(evt);
    });
    _jitsi.addEventListener('participantLeft', evt => {
      console.log('participantLeft');
      console.log(evt);
    });
    _jitsi.addEventListener('videoConferenceJoined', evt => {
      console.log('videoConferenceJoined');
      console.log(evt);
    });
    _jitsi.addEventListener('videoConferenceLeft', evt => {
      console.log('videoConferenceLeft');
      console.log(evt);
    });

    setJitsi(_jitsi);
  };

  React.useEffect(() => {
    initialiseJitsi();

    return () => jitsi?.dispose?.();
  }, []);

  return <div id={jitsiContainerId} style={{ height: 700, width: "100%" }} />;
};

export default VideoConference;
